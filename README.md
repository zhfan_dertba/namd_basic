Boltzman NAMD
====================


1. run PWmat  
--------------------
   - Set job=NAMD, set band index [m1, m2] in NAMD_DETAIL to be included in the energy window. 
 
   - The adiabatic wavefunction is needed if charge analysis will be done.
     PWmat will save the wavefunction every 40 MD steps as default.
     If you use a different number, please modify ngio for namd_psi.f90

   - PWmat will generate OUT.NAMD and ugio


2. run NAMD
--------------------
   
         
   - Usually, not all the states from OUT.NAMD are needed for NAMD, and 
         the NAMD may not start from the first step of MD


     read_outNAMD.x will tailor the OUT.NAMD to OUT.NAMD_update with the inputs
         specifying which MD step will be the first step for NAMD and the band 
         range to output to OUT.NAMD_update

     **Input arguments:**

         istart (specify the MD-step istart as the first step in NAMD, i.e. only output OUT.NAMD info after this MD step)
         ibnd1  (band starting range for output OUT.NAMD_update)
         ibnd2  (band ending range for output OUT.NAMD_update)
         nstep  (number of MD steps you want to output)
         nstate (number of states in the energy window for OUT.NAMD, Note: the number of states for OUT.NAMD_update is determined by ibnd1 (or 2).)

     *Note*: for MD simulation, a band range [m1, m2] is specified in etot.input (see flag NAMD_DETAILS). The index there 
     is in terms of DFT band index, i.e. the first state in DFT will be the "1st" state. The bands in such band range 
     window will be computed and stored in OUT.NAMD

     ibnd1 and ibnd2 band-index here is in terms of states in OUT.NAMD. 
     The "1st" state here (ibnd1=1) will refer to the *first state* in OUT.NAMD. If all the states stored in OUT.NAMD will be used 
     for the NAMD post-processing, ibnd1 should be set to 1, and ibnd2 should be set to the number of states in OUT.NAMD.

     *Example*: in etot.input (PWmat simulation) [m1, m2] is set as 880, 1635, which means bands with index from 880 to 1635 (756 states intotal) will be saved 
     to OUT.NAMD. When using read_outNAMD.x, not all 756 states are used for NAMD. Depending on your needs, maybe only the 401-th to 756-th states are needed.
     Here, 401 (or 756) is in terms of the states *within OUT.NAMD* instead of DFT states. In this case, you will run read_outNAMD.x as:

     ```
     ./read_outNAMD.x  0   401  756  2000  756
     ```
             

   - Run NAMD main program

     - run in bash

         ```
         ./namd_dm.x  0 > log.namd
         ```


     - input argument:

         The first argument "0" is to specify the program starts from scratch.
         When setting this argument to be 1, the program will check NAMD.cc_out, 
             find the last step, and start from there.

     - files needed

         **NAMD.input**
         Here is an example of NAMD.input (for not used argument, specify a random integer number for it):

             1100                ! number of MD steps
             356                 ! number of band in energy window (after read_outNAMD.x tailoring)
             2.0                 ! time length for one MD step
             -1, 300, 0.1        ! hole/electron cooling, Temperature, BoltzWind (not used)
             1                   ! how many steps to print out NAMD.cc_out (better to set to 1)
             401, 756            ! (not used)
             0d0                 ! (not used)
             40                  ! (not used)
             4000                ! ndt: number of dt evolution between two MD steps (needs to be converged)
             100                 ! nit: number of diagonalization between two MD steps (needs to be converged)

         **deco_time**
         This file will specify the decoherence time between two states. 
             Here, only fixed decoherence time is implemented.
         Three column: first and second: band index; last column decoherence time (fs unit)

         Note: for now the first two columns are NOT used, specify any integer.
         For example (number of lines is nstate\*nstate=356\*356):

             1  2   80.0
             1  2   80.0
             1  2   80.0
             1  2   80.0
             ...

         **OUT.NAMD_update**
         This file is generated from read_outNAMD.x

         **NAMD.cc_int** 
         Specify the initial occupation of the charge (only specify the diagonal elements of 
             density matrix). The number of lines should be consistent to nstate (e.g. 356).

         Format:

             Number of states within energy window
             Initial occupation
             Initial occupation
             Initial occupation
             ...

     - file generated

         **NAMD.cc_out** 
         The file saves eigen energy, density matrix, P matrix and phase difference
             for each step (output frequency is controlled by flag in NAMD.input) 
            
         **NAMD.graph.aveE**
         This file saves the averaged energy of the hole/electron in terms of
             time. (eV unit for energy)

         **NAMD.graph.eigen**
         This file saves the eigen energy of the hole/electron for each step
             (output frquency is controlled by print_every_nit in namd_dm.x, eV unit for energy)

     - convergence

         Before production run, it is better to converge small dt step (ndt) between two MD step.
         It is also needed to converge number of diagonalization between two MD step (nit).

     - *Note*

         set `export OMP_NUM_THREADS=xx` since this is a multithread program.
         Usually, set this variable to the number of physical cores of one node for good performance.


3. run NAMD analysis
--------------------


   - Run namd_psi.x

       ```
       ./namd_psi.x 0 0 4 > log.charge
       ```

   - input argument

           istep_plot          ! Since one MD trajectory is used repeately for different NAMD 
                               !  simulations (they start at different steps in MD traj), istep_plot 
                               !  specify which step is used as the 0-th step for NAMD. Since PWmat 
                               !  save wavefunction at every 40 MD steps, NAMD will always starts 
                               !  from a MD step which has its wavefunction saved. 
                               !  istep_plot = the_index_of_this_step/40.
                               ! If other number than 40 is used, then istep_plot = index/nugio
           restart_istep       ! Restart_istep=0 if start from scratch; otherwise, specify the step 
                               !  you want to restart from.
                               !  (this "step" is in terms of NAMD simulation, check the log to see)
           n_sum               ! How often to sum over states to obtain charge density
                               ! Summing over charge density is expensive when system becomes large, default is 4

   - files needed

       **NAMD.input**
       for example (for not used argument, specify a random integer number for it):

           1100                ! number of MD steps
           356                 ! number of band in energy window (after read_outNAMD.x tailoring)
           2.0                 ! time length for one MD step
           -1, 300, 0.1        ! hole/electron cooling, Temperature, BoltzWind (not used)
           1                   ! how many steps to print out NAMD.cc_out (better to set to 1)
           401, 756            ! Since namd_psi will need ugio, specify the band range used in read_outNAMD,
                               !  so that the program will pick correct band from ugio
                               ! This number should be consistent to band range used in read_outNAMD
           0d0                 ! (not used)
           40                  ! (not used)
    
       **ugio**
       Generated by PWmat (adiabatic wavefunction file, this is usually a large file)
    
       **MOVEMENT**
       Generated by PWmat (atomic coordinates collected from MD)
    
       **OUT.GKK**
       Generated by PWmat (G-vectors used in MD, this file should not change during MD if no cell variation is used)
    
       **OUT.NAMD_update**
       Generated by read_outNAMD
    
       **NAMD.cc_out**
       Generated by namd_dm.x

   - *Note*:

       - set `export OMP_NUM_THREADS=xx` since this is a multithread program.
         Usually, set this variable to the number of physical cores of one node for good performance.

       - This calculation becomes expensive when the size of the supercell is big (big grid)
         Optimization is needed.

       - The nugio in PWmat is 40 as default. However, if you use a different number, please
         modify ngio in namd_psi. However, I strongly recommend to check the code carefully 
         since I never used other numbers. Particularly for the  namd_psi part.

       - Remember to set `export OMP_STACKSIZE=512M` (or even higher value), otherwise it will give you a seg fault
         This is because the default size of then stack memory for multithread is not big 
         enough to store big matrix (such as charge density).
