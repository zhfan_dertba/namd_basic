program main

      integer  :: istep2,islda,nkpt,nstep
      integer  :: nstate
      integer  :: kpt_t,iislda_t,mst_win,kpt
      real*8,allocatable,dimension(:) :: E1,E2
      complex*16,allocatable,dimension(:,:) :: cdot, cdot2
      integer mx1,mx2,nstate2, istart, i
      character*256          :: arg_temp

     call getarg(1,arg_temp);  read(arg_temp,*) istart
     call getarg(2,arg_temp);  read(arg_temp,*) mx1
     call getarg(3,arg_temp);  read(arg_temp,*) mx2
     call getarg(4,arg_temp);  read(arg_temp,*) nstep
     call getarg(5,arg_temp);  read(arg_temp,*) nstate

     allocate( E1(nstate) )
     allocate( cdot(nstate,nstate) )

     write(6,*) 'Input band window is from ', mx1, ' to ', mx2
     write(6,*) ' the index here is in terms of states within the window specified in NAMD_details'
     write(6,*) 
     write(6,*) 'Total num of states in OUT.NAMD is ', nstate

      nstate2=mx2-mx1+1
      open(10,file="OUT.NAMD",form="unformatted")
      open(11,file="OUT.NAMD_update",form="unformatted")
      rewind(10)
      rewind(11)

      ! read 1st step of OUT.NAMD  (no cdot info for this step)
        read(10) istep_t,islda,nkpt
        do iislda=1,islda
         do kpt=1,nkpt
          read(10) kpt_t,iislda_t,mst_win
          read(10) E1
         enddo
        enddo
       write(6,*) 'ok'

       if (nstate .ne. mst_win) then
           write(6,*) 'Input total state is not consistent to OUT.NAMD'
           stop
       end if

      ! read following steps of OUT.NAMD
       do i=1,istart
           read(10) istep_t,islda,nkpt
           do iislda=1,islda
               do kpt=1,nkpt
                   read(10) kpt_t,iislda_t,mst_win
                   read(10) cdot
                   read(10) E1
               enddo
           enddo
       end do

       write(6,*) 'Writing first step (no cdot info), starting from  step ', istart+1
 
       write(11) istep_t,islda,nkpt
       write(11) kpt_t,iislda_t,nstate2
       write(11) E1(mx1:mx2)

       write(6,*) 'Writing following steps '

       do i=1,nstep-1
        read(10) istep_t,islda,nkpt
        write(11) istep_t,islda,nkpt
        do iislda=1,islda
         do kpt=1,nkpt
          read(10) kpt_t,iislda_t,mst_win
          write(11) kpt_t,iislda_t,nstate2
          read(10) cdot
          write(11) cdot(mx1:mx2,mx1:mx2)
          read(10) E1
          write(11) E1(mx1:mx2)
         enddo
        enddo
       enddo

      close(10)
      close(11)
end
