RUN steps:
====================

1. run read_outNAMD to extract states you want from OUT.NAMD (in most cases, you may not want all the states in NAMD)

    e.g. 

        ./read_outNAMD.x 0 401 756 2000 756    
        # 0: start from the very first step
        # 401-756: band window you want to extract (index is counted using band window in NAMD_details as the first state)
        # 2000: num of steps 
        # 756: total states in OUT.NAMD


2. double check all the num of states in NAMD.input, NAMD.cc_init, deco_time are consistent to what you have specified and OUT.NAMD_update


3. Run ./namd_dm.x  0
