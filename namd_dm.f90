!
!
! compilation:
!
!
!
!
!
program NAMD_BASIC

     use omp_lib

     implicit none

     complex*16,parameter   :: comi=dcmplx(0.d0,1.d0)
     real*8,parameter       :: hbar=0.6582119514d0
     real*8,parameter       :: hbar_ev_fs=0.6582119514d0
     real*8,parameter       :: t_au_to_fs=0.02418884326d0

     complex*16             :: cc3,csum,cc2,cc,cmax,csum2,cc1,csum3,ctheta, buff, Pij_diag_tmp
     real*8                 :: E,d1,d2,dx(3), temp, win_Boltz, akT, dt
     integer,dimension(3)   :: idx
     character*20           :: filename,filename2,filename3,filename4
     character*1            :: V,U
     character*256          :: arg_temp

     integer                                   :: ndt, nstep,nstate, idt, ie_h, kpt, nout_cc, istep,istep_t,istep2, LWORK, n, nstate2
     integer                                   :: nkpt, islda, iislda, kpt_t,iislda_t, INFO, mst_win, restart, starting_istep, nstate_t
     real*8,allocatable,dimension(:)           :: E1,E2,ET1,ET2, RWORK,W, ET, E1_old
     real*8,allocatable,dimension(:,:)         :: W_nit, Hij_debug
     complex*16,allocatable,dimension(:)       :: cc_int,WORK, cphase, cphase_old
     complex*16,allocatable,dimension(:,:)     :: cdot,AA,HH,DD1,DD2, HHt, HH1, HH2, HH_tmp, psi_tmp, psi, H1, H2, H3, DM,Pij,Pij_tmp, DM_old, Pij_old
     complex*16,allocatable,dimension(:,:)     :: Hij_R, Hij_R_old, Hij_R_t, cdot_old
     complex*16,allocatable,dimension(:,:,:)   :: psi_nit, HH_nit
     real*8,allocatable,dimension(:,:)         :: tau, DM_diff
     real*8                                    :: sum1,sum2,factij, sum_cc, dt_step,  time2, time3, time, Eave, ddE, negative_dii

     integer                                   :: chunk, nthread, it, nit, nddt, print_every_nit, corr_dii_every_nit, nugio, iplot, istep_plot
     integer                                   :: i, j, k, s, i1,i2, j1, j2, m, m1,m2, io, icount_step, count_step,istep_t_old, icount, ibnd_1, ibnd_2
     complex*16                                :: sum_c, sum_c2, sum_c3, temp_cc
     real*8                                    :: t1, t2, t3, t4, t5, tt1, tt2, tt3, tt4, tt5,  tt6, tt7, alpha


     ! restart:   when restart=0, perform NAMD from scratch
     !            when restart=1, the program will check NAMD.cc_out and determine its last step by itself
     !
     call getarg(1,arg_temp);  read(arg_temp,*)restart   ! restart=0: from scratch; =1: restart
     call getarg(2,arg_temp);  read(arg_temp,*)ndt       ! number of small step (dt) between two MD step (better to be larger than 2000)
     call getarg(3,arg_temp);  read(arg_temp,*)nit       ! number of diaganolization needed between two MD step (better to be larger than 50)
     !
     !
     ! read NAMD.input
     open(408,file='NAMD.input')
       rewind(408)
       read(408,*) nstep                     ! number of steps
       read(408,*) nstate                    ! number of states in the window
       read(408,*) dt_step                   ! dt (fs) for each step (same to etot.input in PWmat)
       read(408,*) ie_h, temp, win_Boltz     ! e(1)/h(-1),temperature,win_B(eV)
       if (abs(ie_h).ne.1) then
           write(6,*) "ie_h in NAMD.input must be 1 (for electron) or -1 (for hole)",ie_h
           stop
       endif
       read(408,*) nout_cc                   ! the interval to output NAMD.cc_out
       read(408,*) ibnd_1, ibnd_2            ! *not used in this version, specify any number* ibnd_1 and ibnd_2 are indexed so that the first state stored in ugio_all is counted as "1" 
       read(408,*) alpha                     ! *not used in this version, specify any number* the pre-factor multiplied to the Hij_R
       read(408,*) nugio                     ! *not used in this version, specify any number* how many MD steps (same in PWmat), ugio info is written
     close(408)
     !
     !
     ! set up 
     nugio=40                                ! *not used in this version, specify any number* every 40 MD steps, ugio (adiabatic wavefunction) is written
     nddt=ndt/nit                            ! number of small step between two diag step
     akT=8.61733d-5*temp                     ! eV/K unit
     !
     print_every_nit=10                      ! every 10 it steps, print to graph and fort.96
     corr_dii_every_nit=10000                ! *not used in this version, specify any number* When total number of e-/h+ is conserved, every corr_dii_every_nit nit-steps, make the sum of Dii to be 1. 
     !
     !
     !
     ! initialization
     n=nstate
     LWORK=3*nstate
     allocate(cc_int(nstate),cdot(nstate,nstate),cdot_old(nstate,nstate))
     allocate(AA(nstate,nstate),HH_tmp(nstate,nstate),HHt(nstate,nstate),HH1(nstate,nstate),HH2(nstate,nstate))
     allocate(HH_nit(nstate,nstate,nit),psi_nit(nstate,nstate,nit))
     allocate(H1(nstate,nstate),H2(nstate,nstate),H3(nstate,nstate))
     allocate(DD1(nstate,nstate),DD2(nstate,nstate),psi(nstate,nstate),psi_tmp(nstate,nstate))
     allocate(ET1(nstate),ET2(nstate),ET(nstate),W(nstate),E1(nstate),E2(nstate),W_nit(nstate,nit))
     allocate(RWORK(LWORK),WORK(LWORK),cphase(nstate))
     allocate(tau(nstate,nstate))
     allocate(DM(nstate,nstate))
     allocate(Pij(nstate,nstate),Pij_tmp(nstate,nstate))
     allocate(E1_old(nstate),cphase_old(nstate),DM_old(nstate,nstate),Pij_old(nstate,nstate))
!    allocate(Hij_R(nstate,nstate),Hij_R_old(nstate,nstate),Hij_debug(nstate,nstate),Hij_R_t(nstate,nstate))
     !     
     !     
     open(31,file='deco_time')
       read(31,*) ((buff,buff,tau(i,j),j=1,nstate),i=1,nstate)
     close(31)
     tau=tau/t_au_to_fs   ! change tau from fs to a.u. 
     !     
     DM=dcmplx(0.d0,0.d0)
     Pij=dcmplx(0.d0,0.d0)
     forall(i=1:nstate) cphase(i)=1d0
     !     
     !     
     !     
     !     
     if (restart==1) then   ! restart, first read cc_out to determine which step to restart
         !
         open(17,file="NAMD.graph.eigen",position='append',form='formatted')
         open(18,file="NAMD.graph.aveE",position='append',form='formatted')
         !
         write(6,*) 'restart from previous run, extracting cc_out '
         !
         icount_step=0
         istep_t_old=0
         open(29,file="NAMD.cc_out",form="unformatted")
           rewind(29)
           ! first step cc is written instead of DM
           read(29) istep_t,nstate_t
           if(nstate_t.ne.nstate) then
               write(6,*) "nstates in NAMD.cc_out,NAMD.input not the same", nstate_t,nstate
               stop
           endif
           read(29) E1      ! eV unit
           read(29) cc_int
           read(29) cphase
!          read(29) Hij_R
           io=0
           !
           do while(io==0)
               read(29,iostat=io) istep_t,nstate_t
               if(nstate_t.ne.nstate) then
                   write(6,*) "nstates in NAMD.cc_out,NAMD.input not the same", nstate_t,nstate
                   stop
               endif
               read(29,iostat=io) E1
               read(29,iostat=io) DM
               read(29,iostat=io) Pij
               read(29,iostat=io) cphase
!              read(29,iostat=io) Hij_R
               if (io==0) then
                   istep_t_old=istep_t
                   E1_old=E1
                   DM_old=DM
                   Pij_old=Pij
!                  Hij_R_old=Hij_R
                   cphase_old=cphase
                   icount_step=icount_step+1
               end if
           end do
         close(29)
         count_step=icount_step
         write(6,*) 'extracted last istep of MD from cc_out is ', istep_t_old
         write(6,*) 'total number of saved cc_out from previous run is ', count_step
         istep_t=istep_t_old
         if (istep_t_old==0) then
             write(6,*) "Didn't save cc_out for 1st step, must start from scratch"
             stop
         end if
         E1=E1_old/27.211396d0   ! cc_out energy has eV unit, but here, hartree unit is used
         DM=DM_old
         Pij=Pij_old
!        Hij_R=Hij_R_old
         !
         ! determine the starting MD step
         starting_istep=istep_t+1
         !
         !
     else if (restart==0) then   ! start from scratch
         !
         open(17,file="NAMD.graph.eigen",position='append')
         rewind(17)
         open(18,file="NAMD.graph.aveE",position='append')
         rewind(18)
         !
         ! determine the starting MD step
         starting_istep=1
         !
         !
         ! cc_int(i) is the amplitude on phi_i at the initial point
         !  i.e., psi(t=0)=\sum_i cc_int(i)*phi_i(t=0)*cphase(i)
         !  here psi_i are eigen states
         !
         ! read initial wavefunction coefficient
         open(13,file="NAMD.cc_init")
           rewind(13)
           read(13,*) nstate2
           if(nstate2.ne.nstate) then
               write(6,*) "nstate2 in NAMD.cc /= nstate in NAMD.input,stop"
               write(6,*) "nstate2,nstate",nstate2,nstate
               stop
           endif
           sum_cc=0.d0
           do i=1,nstate
               read(13,*) cc_int(i)
               sum_cc=sum_cc+abs(cc_int(i))**2
           enddo
           sum_cc=1.d0/dsqrt(sum_cc)
           cc_int=cc_int*sum_cc
         close(13)
         !
         forall(i=1:nstate) DM(i,i)=cc_int(i)
         Pij=DM/2d0
         ! 
         ! 
         !
     else
         !
         write(6,*) 'Unknown restart info'
         stop
         !
     end if
     !
     !
     !
     !
     ! read in the E(i) and cdot(i1,i2) from OUT.NAMD for every step
     open(10,file="OUT.NAMD_update",form="unformatted")
     rewind(10)
     !
     !
     ! read initial step
     read(10) istep2,islda,nkpt    ! istep2=0 for t=0fs MD
     if(islda.ne.1.or.nkpt.ne.1) then
         write(6,*) 'currently, only works for islda=1,nkpt=1',islda,nkpt
         stop
     endif
     !
     do iislda=1,islda
         do kpt=1,nkpt
             read(10) kpt_t,iislda_t,mst_win
             if(mst_win.ne.nstate) then
               write(6,*) "nstate.ne.mst_win",nstate,mst_win
               stop
             endif
             read(10) E1   ! hartree unit
         enddo
     enddo
     !
     ! restart
     if (restart==1) then
         icount_step=0
         do while(icount_step<starting_istep-1)
             read(10) istep_t,islda,nkpt 
             do iislda=1,islda
                 do kpt=1,nkpt
                     read(10) kpt_t,iislda_t,mst_win
                     read(10) cdot
                     read(10) E1    ! hartree unit
                 enddo
             enddo
             icount_step=icount_step+1
         end do
         write(6,*) 'extracted last istep of previous run of MD from OUT.NAMD is ', istep_t
         !
         ! last step cdot push to cdot_old
         cdot_old=cdot
     end if
     !
     !
     !
     ! read Hij_R from HijR.dat using starting_istep
     !open(12,file="HijR.dat",form="unformatted")
     !  rewind(12)
     !
     !  if (restart==1) then
     !      do iplot=1, int((starting_istep-1-1)/nugio)+1
     !          read(12) istep_t, istep_plot 
     !          read(12) ((Hij_R_old(m,n), m=1,nstate),n=1,nstate)
     !      end do
     !  end if
     !
     !if (allocated(Hij_R_old)) deallocate(Hij_R_old)
     !
     !
     !
     !
     ! open file for cc_out and write the initial step
     if (restart==0) then
         open(29,file="NAMD.cc_out",form="unformatted",status="replace")
         ! NOTE: first step (t=0fs MD) write cc_int instead of DM into cc_out
         istep=0
         write(29) istep,nstate
         write(29) E1*27.211396d0    ! written energy in eV unit
         write(29) cc_int
         write(29) cphase
!        write(29) Hij_R
     else
         open(29,file="NAMD.cc_out",form="unformatted",position="append")
     end if
     !
     !
     !
     !
     time=(starting_istep-1)*dt_step
     !
     if (restart==1) write(6,*) 'Starting next MD step ', starting_istep
     !
     !
     !
     !               
     !               
     !               
     ! BIG MD STEP
     !               
     do istep=starting_istep,nstep-1


         ! *not used in this version*
         ! first, use cdot_old to rotate wavefunction to obtain Hij_R
         !
         ! when charge is not converved: *not used in this version*
         ! note that: for each MD step when save to cc_out, 
         !     e.g. at end of MD step T_N, Hij_R on basis T_N-1 is saved here. 
         !  Hij_R read from HijR.dat is \int_omega alpha(r)*phi_i^*(r)*phi_j(r) dr,
         !     multiplied by pre-factor and -i is done HERE.
         !
         !  if istep%nugio ==0, read Hij_R.dat data
         !if (mod(istep-1,nugio)==0)  then
         !    read(12) istep_t, istep_plot
         !    read(12) ((Hij_R(m,n), m=1,nstate),n=1,nstate)
         !    Hij_R=Hij_R*(-comi)*alpha
         !else
         !    ! ortho and rotate wavefunction Hij_R
         !    cdot_old=ortho(ie_h,cdot_old,nstate)
         !
         !    call ZGEMM('C', 'N', nstate, nstate, nstate, 1d0, cdot_old, nstate, Hij_R, nstate, 0d0, HH_tmp, nstate)
         !    call ZGEMM('N', 'N', nstate, nstate, nstate, 1d0, HH_tmp, nstate, cdot_old, nstate, 0d0, Hij_R, nstate)
         !end if
         !
         ! for debug
         !  forall(m=1:nstate,n=1:nstate) Hij_debug(m,n)=conjg(Hij_R(m,n))*Hij_R(m,n)
         !  write(6,*) '==> ', maxval(Hij_debug)
         ! *not used in this version*
     
         

         ! read OUT.NAMD as cdot
         ! This istep is working on the time interview: [T1,T2], 
         ! Which corresponds to [E1,E2] and
         ! cdot(i1,i2)=<psi_i1(T1,E1)|psi_i2(T2,E2)>
         !
         read(10) istep_t,islda,nkpt 
         !
         do iislda=1,islda
             do kpt=1,nkpt
                 read(10) kpt_t,iislda_t,mst_win
                 read(10) cdot
                 read(10) E2    ! hartree unit
             enddo
         enddo
         !
         cdot_old=cdot   ! overwrite cdot_old
         !
         ! cdot(i,j)=<phi_i(T1)|phi_j(T2)>
         ! cphase is accumulative, passed on from last iteration. 
         !  phi_i1(T1)=phi_i1(T1)*cphase(i1)
         forall(i1=1:nstate,i2=1:nstate) cdot(i1,i2)=cdot(i1,i2)*dconjg(cphase(i1))


         !  do Grand-Schmidth orthogonalization to force unitarity
         !  **for phi_i2(T2),using phi_i1(T1) as the basis**
         !     make cdot a unitary matrix
         cdot=ortho(ie_h,cdot,nstate)



         ! compute H(T2) with phi_(T1) basis
         !
         ! define: AA(i1,i2)=<phi_i1(T1)|(H(t2)-H(t1))|phi_i2(T1)>
         ! H(T2)=sum_i2 |phi_i2(T2)> E2(i2) <phi_i2(T2)|
         ! H(T1)=sum_i1 |phi_i1(T1)> E1(i1) <phi_i1(T1)|
         !
!$OMP PARALLEL SHARED(nstate,cdot,E2,AA,E1,nthread,chunk)  &
!$OMP PRIVATE(i1,i2,cc2,i) 
         nthread = OMP_GET_MAX_THREADS()
         chunk = ceiling( (nstate*nstate)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK) COLLAPSE(2)
         do i1=1,nstate
             do i2=1,nstate
                 cc2=dcmplx(0.d0,0.d0)
                 do i=1,nstate
                     cc2=cc2+cdot(i1,i)*dconjg(cdot(i2,i))*E2(i)
                 enddo
                 AA(i1,i2)=cc2                ! AA is H(t2) under phi_T1 basis
                 if(i1.eq.i2) then
                     AA(i1,i2)=AA(i1,i2)-E1(i1)   ! E1 is H(T1)
                 endif
             enddo
         enddo
!$OMP END DO NOWAIT
!$OMP END PARALLEL
         !
         AA=(AA)/(dt_step/t_au_to_fs)       ! so H(t) will be H(t)=H(T1)+AA*(t-T1), under phi_T1 basis
         !
         !
         ! Note, the H(T2), hence AA, does not depend on the phase of phi_T2. 
         !  However, at the end of the [T1,T2]
         ! integration, the phase of phi_i2(T2,E2) will be fixed, and the
         ! phase change is stored inside cphase, which will be used to
         ! fix the phase of phi_i1(T1,E1) for the next istep




         ! DD1 is initialized to store last diag-step wavefunction (with T1 basis)
         DD1=cmplx(0d0,0d0)
         forall(i=1:nstate) DD1(i,i)=1d0
 
!        Hij_R_t=cmplx(0d0,0d0)

         ET1=E1




         dt=dt_step/ndt   ! dt is fs unit, dt is the small time step for every small time-evolution
  


         ! diag all matrix before evolution
         !  between two MD step T1 and T2, a serial of diag is performed on the time from t0 to t_nit
         !  Hamiltonian and the diag-eigen vector are all in T1 basis
         !
!$OMP PARALLEL SHARED(nit,nddt,dt,nstate,HH_nit,E1,psi_nit,AA,W_nit,LWORK,nthread,chunk)  &
!$OMP PRIVATE(it,time2,i,j,W,WORK,RWORK,INFO,psi)
         nthread = OMP_GET_MAX_THREADS()
         chunk = ceiling( nit/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
         do it=1,nit
             !
             ! time2 is the time for the diag, i.e. time2=t-t1
             time2=it*nddt*dt
             !
             ! updat H(t) to time2
             ! H(t) = H(t1) + (t-t1)/(t2-t1)*(H(t2)-H(t1)), note Ham is written in \phi_T1 basis; HH is H(t)
             do i=1,nstate
                 do j=1,nstate
                     if (j==i)then
                         HH_nit(i,j,it)=E1(i)+time2/t_au_to_fs*AA(i,j)
                     else
                         HH_nit(i,j,it)=time2/t_au_to_fs*AA(i,j)
                     end if
                 end do
             end do
             psi=HH_nit(:,:,it)
             !
             ! all the eigenvectors are in psi array
             call zheev('V','U',nstate,psi,nstate,W,WORK,LWORK,RWORK,INFO)
             psi_nit(:,:,it)=psi
             !
             ! all the eigenvalues are in W_nit array
             W_nit(:,it)=W
             !
         end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL




         ! start diag-iter
         do it=1,nit

 
 
             ! save eigen vector to DD2 (DD2 is in T1 basis)
             DD2=psi_nit(:,:,it)
             !
             !
             ! modulate DD2 phase based on DD1
!$OMP PARALLEL SHARED(nstate,DD1,DD2,nthread,chunk)  &
!$OMP PRIVATE(m1,m2,i,cc,cmax)
             nthread = OMP_GET_MAX_THREADS()
             chunk = ceiling( nstate/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
             do m1=1,nstate
                 cmax=dcmplx(0.d0,0.d0)
                 do m2=1,nstate
                     cc=dcmplx(0.d0,0.d0)
                     do i=1,nstate
                         cc=cc+dconjg(DD1(i,m2))*DD2(i,m1)
                     enddo
                     if (abs(cc).gt.abs(cmax)) then! to find the maximum overlap, m2 at DD1(i,m2) (beginning of this time strp) for state m1 at this time t (DD2(i,m1)), 
                         cmax=cc
                     endif
                 enddo
                 cmax=dconjg(cmax)/abs(cmax)       ! Use the phase of DD1(i,m2) to redefine the phase of DD2(i,m1) 
                 do i=1,nstate
                     DD2(i,m1)=DD2(i,m1)*cmax
                 enddo
             enddo
!$OMP END DO NOWAIT
!$OMP END PARALLEL

 

             ! copy energy to ET2 for diag-step it
             ET2=W_nit(:,it)
 


             ! interpolate HH1 and HH2 along nddt*dt
             ! with |phi_(t+nddt*dt)> basis, HH1 will be diagonal (ET1), HH2 needs basis change
             !            
             HH1=cmplx(0d0,0d0)
             forall(i=1:nstate) HH1(i,i)=ET1(i)
             !
             !
             ! copy Ham to HH2 for interpolation (HH2 is \phi(T1) basis)
             HH2=HH_nit(:,:,it)
             !
             !
             ! rotate HH2 with \phi_T1 basis to \phi_t1 basis 
             call ZGEMM('C', 'N', nstate, nstate, nstate, 1d0, DD1, nstate, HH2, nstate, 0d0, HH_tmp, nstate)
             call ZGEMM('N', 'N', nstate, nstate, nstate, 1d0, HH_tmp, nstate, DD1, nstate, 0d0, HH2, nstate)

 
             ! *not used in this version*
             ! rotate Hij_R with \phi_T1 basis to \phi_t1 basis
             !
!            call ZGEMM('C', 'N', nstate, nstate, nstate, 1d0, DD1, nstate, Hij_R, nstate, 0d0, HH_tmp, nstate)
!            call ZGEMM('N', 'N', nstate, nstate, nstate, 1d0, HH_tmp, nstate, DD1, nstate, 0d0, Hij_R_t, nstate)
             ! *not used in this version*


  
             ! evolve small dt within two 'it' steps
             do idt=1,nddt
  
                 ! time3 is the time for the small dt
                 time3=(it-1)*nddt*dt+(idt-1)*dt
 

                 ! updat H(t) to time3, interpolate H(t) between two diag
                 ! H(t) = H(t1) + (t-t1)/(t2-t1)*(H(t2)-H(t1)), note Ham is written in \phi_t1 basis; HHt is H(t)
!                forall(i=1:nstate,j=1:nstate) HHt(i,j)=(HH2(i,j)-HH1(i,j))/(nddt*dt)*(idt-1)*dt + HH1(i,j) + Hij_R_t(i,j)
                 forall(i=1:nstate,j=1:nstate) HHt(i,j)=(HH2(i,j)-HH1(i,j))/(nddt*dt)*(idt-1)*dt + HH1(i,j)

 
                 ! update E(t)
                 ET=ET1+(ET2-ET1)/(dt*nddt)*((idt-1)*dt)

  
                 ! update  Pij
                 !
                 ! evolve diagonal term of DM
                 !  expand DM to forth-order of dt^4
                 !  D(t+dt) = D(t) -idt*(HD-DH) 
                 !                 -dt^2/2*(HHD+DHH) + dt^2(HDH )
                 !                 +i/6*dt^3(HHHD-DHHH) + i/2*dt^3(HDHH-HHDH)
                 !                 +dt^4/24(HHHHD+DHHHH) - dt^4/6(HHHDH+HDHHH) + dt^4/4(HHDHH)

                 ! evolve off-diagonal term of DM (Pij)
                 ! 1st order evolution - 
                 Pij_tmp=Pij
                 call ZGEMM('N', 'N', nstate, nstate, nstate, 1d0, HHt, nstate, Pij_tmp, nstate, 0d0, H1, nstate)
                 call ZGEMM('N', 'C', nstate, nstate, nstate, 1d0, Pij_tmp, nstate, HHt, nstate, 0d0, H2, nstate)
                 forall(i=1:nstate,j=1:nstate) Pij(i,j)=Pij_tmp(i,j)-comi*dt/t_au_to_fs*(H1(i,j)-H2(i,j))

                 !
                 ! * not used in this version *
                 !call ZGEMM('N', 'N', nstate, nstate, nstate, 1d0, Hij_R_t, nstate, Pij_tmp, nstate, 0d0, H1, nstate)
                 !call ZGEMM('N', 'N', nstate, nstate, nstate, 1d0, Pij_tmp, nstate, Hij_R_t, nstate, 0d0, H2, nstate)
                 !forall(i=1:nstate,j=1:nstate) Pij(i,j)=Pij_tmp(i,j)-comi*dt/t_au_to_fs*(H1(i,j)-H2(i,j))
                 !

                 ! 2nd order evolution -
                 !H3=c_matmul(nstate,H1,HHt)
                 !H1=c_matmul(nstate,HHt,H1)
                 !H2=c_matmul(nstate,H2,HHt)
                 !forall(i=1:nstate,j=1:nstate) Pij(i,j)=Pij(i,j)-0.5d0*(dt/t_au_to_fs)**2*(H1(i,j)+H2(i,j))+(dt/t_au_to_fs)**2*H3(i,j)

                 !
             ! loop end for small dt within two diag steps
             end do


             ! for debug *not used in this version*
             !
             !   forall(i=1:nstate) DM(i,i)=2d0*Pij(i,i)
             ! forall(i=1:nstate,j=1:nstate,i/=j) DM(i,j)=Pij(i,j)+conjg(Pij(j,i))
             !   sum_c=cmplx(0d0,0d0)
             !   do i=1,nstate
             !       sum_c=sum_c+DM(i,i)
             !   end do
             !   write(6,*) 'End first diag, ', istep, it, sum_c
             ! H1=c_matmul(nstate,Hij_R_t,DM)
             ! sum_c=cmplx(0d0,0d0)
             ! do m=1,nstate
             !     sum_c=sum_c+H1(m,m)
             ! end do
             ! write(6,*) 'reduced charge is ', 2d0*sum_c*dt*nddt/t_au_to_fs*comi
             !   stop


             ! Rotation of basis for Pij
             !
             ! compute basis-transformation matrix <phi_t1|phi_t2>
             call ZGEMM('C', 'N', nstate, nstate, nstate, 1d0, DD1, nstate, DD2, nstate, 0d0, psi_tmp, nstate)
             !
             !
             ! rotate basis of Pij from phi_t1 to phi_t2
             call ZGEMM('C', 'N', nstate, nstate, nstate, 1d0, psi_tmp, nstate, Pij, nstate, 0d0, Pij_tmp, nstate)
             call ZGEMM('N', 'N', nstate, nstate, nstate, 1d0, Pij_tmp, nstate, psi_tmp, nstate, 0d0, Pij, nstate)


             Pij_tmp=Pij



             ! compute decoherence of Pij
             !
!$OMP PARALLEL SHARED(nstate,Pij,Pij_tmp,psi_tmp,nddt,dt,tau,nthread,chunk)  &
!$OMP PRIVATE(i,j)
             nthread = OMP_GET_MAX_THREADS()
             chunk = ceiling( (nstate*nstate)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK) COLLAPSE(2)
             do i=1,nstate
                 do j=1,nstate
                     if (i==j) cycle
                     Pij(i,j)=Pij_tmp(i,j) -psi_tmp(i,j)*Pij_tmp(i,i)+conjg(Pij_tmp(j,j))*conjg(psi_tmp(j,i))-Pij_tmp(i,j)*(nddt*dt/t_au_to_fs)/tau(i,j)
                 end do
             end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL
             !
             Pij_tmp=Pij



             ! compute cooling of Pij
             !
!$OMP PARALLEL SHARED(nstate,Pij,Pij_tmp,psi_tmp,akT,ie_h,nthread,chunk)  &
!$OMP PRIVATE(i,j,csum,ddE,factij)
             nthread = OMP_GET_MAX_THREADS()
             chunk = ceiling( (nstate)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK) 
             do i=1,nstate
                 csum=cmplx(0d0,0d0)
                 do j=1,nstate
                     if (i==j) cycle
                     ddE=(ET(j)-ET(i))*27.211396d0   ! because akT is eV unit
                     if (ie_h==1) then  ! electron cooling
                         if(ddE .gt. 0) factij=0.d0
                         if(ddE .lt. 0) factij=1.d0
                     else if (ie_h==-1) then  ! hole cooling
                         if(ddE .gt. 0) factij=1.d0
                         if(ddE .lt. 0) factij=0.d0
                     end if
                     csum=csum+real(Pij_tmp(i,j)*psi_tmp(j,i))*     factij *(exp(-abs(ddE)/akT)-1d0)  &
                              -real(Pij_tmp(j,i)*psi_tmp(i,j))*(1d0-factij)*(exp(-abs(ddE)/akT)-1d0)
                 end do
                 Pij(i,i)=Pij_tmp(i,i)+csum
             end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL




             ! generate Dij
             forall(i=1:nstate) DM(i,i)=2d0*real(Pij(i,i))
             forall(i=1:nstate,j=1:nstate,i/=j) DM(i,j)=Pij(i,j)+conjg(Pij(j,i))


                  
             ! correct the negative value of Dii
             !  D_ii' = D_ii*(1+nega_val/(1-nega_val))
             !   so that  \sum_i D_ii' is still = 1.0
             negative_dii=0d0
             sum_c=cmplx(0.d0,0d0)
             do i=1,nstate
                 if (dreal(DM(i,i)) .lt. -1.d-20) then
                     negative_dii=negative_dii+dreal(DM(i,i))
                     DM(i,i)=dcmplx(0.d0,0.d0)
                     Pij(i,:)=dcmplx(0.d0,0.d0)
                     Pij(:,i)=dcmplx(0.d0,0.d0)
                 endif
                 sum_c=sum_c+DM(i,i)
             enddo
             sum_c2=cmplx(0.d0,0d0)
             sum_c3=cmplx(0.d0,0d0)
             do i=1,nstate
                 DM(i,i)=DM(i,i)+dreal(DM(i,i))*negative_dii/dreal(sum_c)
                 temp_cc=Pij(i,i)
                 Pij(i,:)=Pij(i,:)*(1.0d0+negative_dii/dreal(sum_c))
                 Pij(:,i)=Pij(:,i)*(1.0d0+negative_dii/dreal(sum_c))
                 Pij(i,i)=temp_cc*(1.0d0+negative_dii/dreal(sum_c))
                 sum_c2=sum_c2+DM(i,i)
                 sum_c3=sum_c3+dreal(DM(i,i))*negative_dii/dreal(sum_c)
             end do
! for debug: write(6,'(5E20.12)') negative_dii, dreal(sum_c2), dreal(sum_c), dreal(sum_c3)
            !write(6,'(5E20.12)') negative_dii, dreal(sum_c2), dreal(sum_c), dreal(sum_c3)

             ! 
             ! copy energy/wavefunction to ET1/DD1
             ET1=ET2
             DD1=DD2



            !! correct diag element by summing over Pij(i,i)
            !if (mod(it,corr_dii_every_nit)==0) then
            !    sum_c=cmplx(0.d0,0d0)
            !    do i=1,nstate
            !        sum_c=sum_c+Pij(i,i)+conjg(Pij(i,i))
            !    enddo
            !    Pij=Pij/sum_c
            !end if



             ! print diag-term of DM
             if (mod(it,print_every_nit)==0) then
                 write(96,*) time3+dt+time
                 do i=1,nstate
                     write(96,'(2E20.12)') dreal(DM(i,i)), dimag(DM(i,i))
                 end do
             end if
             !
             !
             ! print to graph files
             if (mod(it,print_every_nit)==0) then
                 sum1=0.d0
                 sum_c=cmplx(0.d0,0d0)
                 Eave=0.d0
                 do m=1,nstate
                     sum1=sum1+DM(m,m)
                     Eave=Eave+ET1(m)*abs(DM(m,m))
                     sum_c=sum_c+DM(m,m)
                 enddo
                 Eave=Eave/sum1
                 write(6,'(A,F13.4,I10,I10,2F13.4)') 'Working on time(fs), MD istep, diag-step it, norm_info:', (istep-1)*dt_step+time3+dt, istep, it, sum_c
                 write(17,*) (istep-1)*dt_step+time3+dt
                 write(17,*) (ET1(i)*27.211396d0,i=1,nstate)
                 write(18,'(f25.10,2x,5(E15.8,1x))') (istep-1)*dt_step+time3+dt,Eave*27.211396d0
             endif


         ! loop end for re-diag steps
         end do



         ! cphase(m) is the phase of <phi_m(T2) | phi'_m(T1)> ,  phi'_m is from DD1 (or DD2), phi_m is from cdot
         ! the current coefficient cc_int(i) is based on HH(i,m), but for next istep, the 
         ! overlap is based on cdot(i,m). So, we have to correct the cdot(i,m) in the next istep. 
         do m=1,nstate
             cc=dcmplx(0.d0,0.d0)
             do i=1,nstate
                 cc=cc+DD1(i,m)*dconjg(cdot(i,m))
             enddo
             cphase(m)=cc/abs(cc)
         enddo

         E1=E2

         
         ! Note that for Hij_R, e.g. here when write to cc_out at the end of MD step T_N, 
         !     Hij_R written here is on the basis of T_N-1
         !     DM, Pij are written on the basis of T_N
         !
         if (nout_cc.gt.0) then
             if (mod(istep,nout_cc).eq.0) then
                 write(29) istep,nstate
                 write(29) E1*27.211396d0
                 write(29) DM
                 write(29) Pij
                 write(29) cphase
!                write(29) Hij_R
             endif
         endif


         time=time+dt_step


     ! loop end for MD steps
     end do

     close(12)

contains


    function c_matmul(nstate,H1,H2)
      integer                              :: nstate
      complex*16,dimension(nstate,nstate)  :: H1
      complex*16,dimension(nstate,nstate)  :: H2
      complex*16,dimension(nstate,nstate)  :: c_matmul

      integer                              :: i, j, k, nthread, chunk
      complex*16                           :: csum
      complex*16,dimension(nstate,nstate)  :: H_tmp

!$OMP PARALLEL SHARED(nstate,H1,H2,H_tmp,nthread,chunk)  &
!$OMP PRIVATE(i,j,k,csum)
      nthread = OMP_GET_MAX_THREADS()
      chunk = ceiling( (nstate*nstate)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK) COLLAPSE(2)
      do i=1,nstate
          do j=1,nstate
              csum=cmplx(0d0,0d0)
              do k=1,nstate
                  csum=csum+H1(i,k)*H2(k,j)
              end do
              H_tmp(i,j)=csum
          end do
      end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

      forall(i=1:nstate,j=1:nstate) c_matmul(i,j)=H_tmp(i,j)

    end function

    function c_matmul_tc(nstate,H1,H2)
      integer                              :: nstate
      complex*16,dimension(nstate,nstate)  :: H1
      complex*16,dimension(nstate,nstate)  :: H2
      complex*16,dimension(nstate,nstate)  :: c_matmul_tc

      integer                              :: i, j, k, nthread, chunk
      complex*16                           :: csum
      complex*16,dimension(nstate,nstate)  :: H_tmp

!$OMP PARALLEL SHARED(nstate,H1,H2,H_tmp,nthread,chunk)  &
!$OMP PRIVATE(i,j,k,csum)
      nthread = OMP_GET_MAX_THREADS()
      chunk = ceiling( (nstate*nstate)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK) COLLAPSE(2)
      do i=1,nstate
          do j=1,nstate
              csum=cmplx(0d0,0d0)
              do k=1,nstate
                  csum=csum+conjg(H1(k,i))*H2(k,j)
              end do
              H_tmp(i,j)=csum
          end do
      end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

      forall(i=1:nstate,j=1:nstate) c_matmul_tc(i,j)=H_tmp(i,j)

    end function


    ! Gram-Schmit to force unitarity
    function ortho(ie_h,cdot,nstate)
      integer                              :: nstate, ie_h
      complex*16,dimension(nstate,nstate)  :: cdot
      complex*16,dimension(nstate,nstate)  :: ortho

      integer                              :: j2, j1, i
      complex*16                           :: cc3
      real*8                               :: sum_cc

      ortho=cdot

      if (ie_h==1) then   ! electron case
          do j2=1,nstate
              do j1=1,j2-1
                  cc3=dcmplx(0.d0,0.d0)
                  do i=1,nstate
                      cc3=cc3+dconjg(ortho(i,j1))*ortho(i,j2)
                  enddo
                  do i=1,nstate
                      ortho(i,j2)=ortho(i,j2)-cc3*ortho(i,j1)
                  enddo
              enddo
              sum_cc=0.d0
              do i=1,nstate
                  sum_cc=sum_cc+real(ortho(i,j2)*dconjg(ortho(i,j2)))
              enddo
              sum_cc=1.d0/dsqrt(sum_cc)
              do i=1,nstate
                  ortho(i,j2)=sum_cc*ortho(i,j2)
              enddo
          enddo
      else                 ! hole case
          do j2=1,nstate
              do j1=1,j2-1
                  cc3=dcmplx(0.d0,0.d0)
                  do i=1,nstate
                      cc3=cc3+dconjg(ortho(i,nstate-j1+1))*ortho(i,nstate-j2+1)
                  enddo
                  do i=1,nstate
                      ortho(i,nstate-j2+1)=ortho(i,nstate-j2+1)-cc3*ortho(i,nstate-j1+1)
                  enddo
              enddo
              sum_cc=0.d0
              do i=1,nstate
                  sum_cc=sum_cc+real(ortho(i,nstate-j2+1)*dconjg(ortho(i,nstate-j2+1)))
              enddo
              sum_cc=1.d0/dsqrt(sum_cc)
              do i=1,nstate
                  ortho(i,nstate-j2+1)=sum_cc*ortho(i,nstate-j2+1)
              enddo
          enddo
      end if

    end function


end program
