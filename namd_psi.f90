
  ! NOTE:
  !
  ! large stack size is needed for each thread
  !   set environmental variable:
  !   (in bash)
  !       export OMP_STACKSIZE=678M
  !

program NAMD_psi
  use omp_lib

    implicit double precision (a-h,o-z)

    real*8, parameter                        :: A_AU_1=0.52917721067d0
    real*8, parameter                        :: pi=3.141592653589d0
    complex*16, parameter                    :: comi=cmplx(0d0,1d0)

    integer                                  :: nstep,nstate, icount_state, istep,istep_t, mg_nx,istep_plot,mg_nx_t,mst, nnodes,nkpt,islda,kpt,iislda
    real*8                                   :: AL(3,3),AL_t(3,3)
    real*8,allocatable,dimension(:)          :: E1
    complex*16,allocatable,dimension(:)      :: cc_int,cphase, bb
    complex*16,allocatable,dimension(:,:)    :: DM, Pij, Hij_R
    complex*16 cc

    integer                                  :: ngtotnod_9(100,400), n1, n2, n3, restart_istep
    real*8, allocatable,dimension(:)         :: gkk_n_tmp,gkk_n_xtmp, gkk_n_ytmp,gkk_n_ztmp, wrk, E2
    real*8, allocatable,dimension(:)         :: gkk,gkk_x,gkk_y,gkk_z
    real*8,allocatable,dimension(:,:)        :: xatom,xyz
    real*8,allocatable,dimension(:,:,:,:)    :: rho_r, rho_i
    real*8,allocatable,dimension(:,:,:)      :: rhor, rhoi, rho_tmp2
    complex*16,allocatable,dimension(:)      :: ug_n_tmp
    complex*16,allocatable,dimension(:,:)    :: ug_tot, ug_tot_tmp, cdot
    complex*16,allocatable,dimension(:,:,:)  :: rho_tmp, rho1, rho2
    integer,allocatable,dimension(:)         :: iz
    integer,allocatable,dimension(:,:,:)     :: index_ig
    character(len=256)                       :: arg_temp
    character(len=4  )                       :: istep_index

    integer                                  :: chunk, nthread, i1, i2, i3, i, j, k, m, n, ibnd_1, ibnd_2, ngio, ng, j1, j2, time4, time5, clock_max, n_sum
    real*8                                   :: sum_r, aa1, aa2, aa3, clock_rate
    complex*16                               :: sum_c, inner

    call getarg(1,arg_temp);  read(arg_temp,*) istep_plot       ! Since one MD trajectory is used repeately for different NAMD simulations (they start at different steps in MD traj), 
                                                                !  istep_plot specify which step is used as the 0-th step for NAMD. Since PWmat save wavefunction at every 40 MD steps,
                                                                !  NAMD will always starts from a MD step which has its wavefunction saved. istep_plot = the_index_of_this_step/40.
    call getarg(2,arg_temp);  read(arg_temp,*) restart_istep    ! restart_istep=0 if start from scratch; otherwise, specify the step you want to restart from in terms of NAMD step
    call getarg(3,arg_temp);  read(arg_temp,*) n_sum            ! how often to sum over states to obtain charge density

    ngio=40   ! in PWmat every 40 MD steps, save adiabatic wfc, set the same number here




    ! read NAMD.input
    open(408,file="NAMD.input")
    rewind(408)
    read(408,*) nstep
    read(408,*) nstate
    read(408,*) dt_step
    read(408,*) ie_h, temp,win_Boltz
    read(408,*) nout_cc
    read(408,*) ibnd_1, ibnd_2                                  ! ibnd_1 and ibnd_2 are indexed so that the first state stored in ugio_all is counted as "1"
    close(408)
    if (ibnd_2-ibnd_1+1 /= nstate) then
        write(6,*) 'inconsistent ibnd_1, ibnd_2 and nstate'
        stop
    end if




    ! read OUT.GKK
    open(11,file="OUT.GKK",form="unformatted")
    rewind(11)
    read(11) n1,n2,n3,mg_nx,nnodes,nkpt,is_SO,islda
    write(6,*) 'Read OUT.GKK', ' n1,n2,n3,mg_nx,nnodes,nkpt,is_SO,islda'
    write(6,*) n1,n2,n3,mg_nx,nnodes,nkpt,is_SO,islda
    read(11) Ecut
    read(11) AL
    AL=AL/A_AU_1
    read(11) nnodes, ((ngtotnod_9(inode,kpt),inode=1,nnodes),kpt=1,nkpt)
    if(nkpt.ne.1) write(6,*) "WARNING, only works for 1-kpt",nkpt

    allocate(gkk_n_tmp(mg_nx))
    allocate(gkk_n_xtmp(mg_nx))
    allocate(gkk_n_ytmp(mg_nx))
    allocate(gkk_n_ztmp(mg_nx))
    allocate(gkk(mg_nx*nnodes))
    allocate(gkk_x(mg_nx*nnodes))
    allocate(gkk_y(mg_nx*nnodes))
    allocate(gkk_z(mg_nx*nnodes))

    do kpt=1,nkpt    ! assume only has one k-point
        num=0
        do inode=1,nnodes
            read(11) gkk_n_tmp
            read(11) gkk_n_xtmp
            read(11) gkk_n_ytmp
            read(11) gkk_n_ztmp
            do ig=1,ngtotnod_9(inode,kpt)
                gkk(num+ig)=gkk_n_tmp(ig)
                gkk_x(num+ig)=gkk_n_xtmp(ig)
                gkk_y(num+ig)=gkk_n_ytmp(ig)
                gkk_z(num+ig)=gkk_n_ztmp(ig)
            enddo
            num=num+ngtotnod_9(inode,kpt)
        enddo
        ng_tot=num
    enddo
    close(11)

    vol=AL(1,1)*(AL(2,2)*AL(3,3)-AL(3,2)*AL(2,3))+ AL(2,1)*(AL(3,2)*AL(1,3)-AL(1,2)*AL(3,3))+ AL(3,1)*(AL(1,2)*AL(2,3)-AL(2,2)*AL(1,3))
    vol=dabs(vol)

    write(6,*) 'volume is ', vol




    ! start reading MOVEMENT
    open(15,file="MOVEMENT")
    rewind(15)
    read(15,*) natom
    allocate(xatom(3,natom))
    allocate(xyz(3,natom))
    allocate(iz(natom))
    read(15,*)
    read(15,*) AL_t(1,1),AL_t(2,1),AL_t(3,1)
    read(15,*) AL_t(1,2),AL_t(2,2),AL_t(3,2)
    read(15,*) AL_t(1,3),AL_t(2,3),AL_t(3,3)
    sum_r=0.d0
    do i=1,3
        do j=1,3
            sum_r=sum_r+abs(AL(i,j)*A_AU_1-AL_t(i,j))
        enddo
    enddo
    if(sum_r.gt.0.01) then
        write(6,*) "AL in MOVEMENT not the same, stop"
        stop
    endif
    rewind(15)



    ! read ugio
    write(6,*) 'start to read ugio_all'
    open(10,file="ugio_all10001",form="unformatted")
    rewind(10)
    read(10) istep_t,mg_nx_t,mst,nnodes,nkpt,islda,kpt,iislda
    write(6,*) 'Read ugio', '  mg_nx_t, mst, nnode, nkpt, islda,kpt,iislda'
    write(6,*)  mg_nx_t,mst,nnodes,nkpt,islda,kpt,iislda
    write(6,*) 'Input nstate from ugio is ', mst
    write(6,*) 'Only use state range: ', ibnd_1 , ibnd_2
    write(6,*) ' (istep_plot=0 will extract initial wfc) '
    if(mg_nx.ne.mg_nx_t) then
        write(6,*) "mg_nx.ne.mg_nx_t, OUT.GKK,ugio_all10001", mg_nx,mg_nx_t
        stop
    endif

    allocate(ug_n_tmp(mg_nx))
    allocate(ug_tot(ng_tot,nstate),ug_tot_tmp(ng_tot,nstate))

    rewind(10)
    ! read 0-th MD step ugio_all
    write(6,*) 'extracting istep_plot from ugio_all ', istep_plot
    num=0
    ug_tot=cmplx(0d0,0d0)
    ! following is the initial step
    read(10) istep_t,mg_nx_t,mst,nnodes,nkpt,islda,kpt,iislda
    write(6,*) ' istep from ugio ', istep_t 
    do inode=1,nnodes
        do m=1,mst
            read(10) ug_n_tmp(:)
            if (m>=ibnd_1 .and. m<=ibnd_2) then
                do ig=1,ngtotnod_9(inode,1)   ! only one kpoint
                    ug_tot(num+ig,m-ibnd_1+1)=ug_n_tmp(ig)
                enddo
            end if
        enddo
        num=num+ngtotnod_9(inode,1)
    enddo

    if (istep_plot>0) then
        do i=1,istep_plot
            ! following is the i-th step
            num=0
            ug_tot=cmplx(0d0,0d0)
            read(10) istep_t,mg_nx_t,mst,nnodes,nkpt,islda,kpt,iislda
            write(6,*) 'extracting istep_plot from ugio_all ', istep_t
            do inode=1,nnodes
                do m=1,mst
                    read(10) ug_n_tmp(:)
                    if (m>=ibnd_1 .and. m<=ibnd_2) then
                        do ig=1,ngtotnod_9(inode,1)   ! only one kpoint
                            ug_tot(num+ig,m-ibnd_1+1)=ug_n_tmp(ig)
                        enddo
                    end if
                enddo
                num=num+ngtotnod_9(inode,1)
            enddo
        end do
    end if


    allocate(rho_tmp(n1,n2,n3),rho_tmp2(n1,n2,n3))
    allocate(rhor(n1,n2,n3),rhoi(n1,n2,n3))
    allocate(rho1(n1,n2,n3),rho2(n1,n2,n3))
    allocate(rho_r(n1,n2,n3,nstate),rho_i(n1,n2,n3,nstate))
        
    !
    if (restart_istep==0) then

        ! perform FFT to adiabatic states
        !
        write(6,*) 'start to do FFT for initial state T=0fs'
        rho_r=0d0; rho_i=0d0
!$OMP PARALLEL SHARED(nstate,ng_tot,n1,n2,n3,AL,gkk_x,gkk_y,gkk_z,rho_r,rho_i,ug_tot,nthread,chunk)  &
!$OMP PRIVATE(m,ig,aa1,aa2,aa3,i1,i2,i3,index_ig,lwrk,wrk) 
        nthread = OMP_GET_MAX_THREADS()
        chunk = ceiling( nstate/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK) 
        do m=1,nstate
            lwrk=6*(n1+n2+n3)+15
            if (.not. allocated(wrk)) allocate(wrk(lwrk))
            if (.not. allocated(index_ig)) allocate(index_ig(n1,n2,n3))
            index_ig=0
            do ig=1,ng_tot
                aa1=AL(1,1)*gkk_x(ig)+AL(2,1)*gkk_y(ig)+AL(3,1)*gkk_z(ig)
                aa2=AL(1,2)*gkk_x(ig)+AL(2,2)*gkk_y(ig)+AL(3,2)*gkk_z(ig)
                aa3=AL(1,3)*gkk_x(ig)+AL(2,3)*gkk_y(ig)+AL(3,3)*gkk_z(ig)
                aa1=aa1/(2*pi)+n1*2+0.1
                aa2=aa2/(2*pi)+n2*2+0.1
                aa3=aa3/(2*pi)+n3*2+0.1
                i1=aa1
                i2=aa2
                i3=aa3
                i1=mod(i1,n1)+1
                i2=mod(i2,n2)+1
                i3=mod(i3,n3)+1
                if(index_ig(i1,i2,i3).gt.0) then
                    write(6,*) "something wrong,two ig,samei1,i2,i3", aa1,aa2,aa3,i1,i2,i3
                    stop
                endif
                index_ig(i1,i2,i3)=1
                rho_r(i1,i2,i3,m)=real(ug_tot(ig,m))
                rho_i(i1,i2,i3,m)=aimag(ug_tot(ig,m))
            end do
            wrk=0d0
            call cfft(n1,n2,n3,rho_r(:,:,:,m),rho_i(:,:,:,m),wrk,lwrk,1)
        end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

        ! analyze adiabatic state
        write(106,*) '#, m- state, total charge, Au charge, GaN charge', 0
        do m=1,nstate
            rhor=dble(rho_r(:,:,:,m)**2+rho_i(:,:,:,m)**2)
            write(106,'(I,4F20.12)') m, sum(rhor)*vol/(n1*n2*n3), sum(rhor(:,:,67:130))*vol/(n1*n2*n3), (sum(rhor(:,:,1:66))+sum(rhor(:,:,131:)))*vol/(n1*n2*n3)
        end do


    end if
 


    ! read cc_out
    write(6,*) 'start to read cc_out'
    allocate(E1(nstate))
    allocate(cc_int(nstate))
    allocate(DM(nstate,nstate))
    allocate(Pij(nstate,nstate))
    allocate(cphase(nstate))
    allocate(Hij_R(nstate,nstate))
    DM=cmplx(0d0,0d0)
    Pij=cmplx(0d0,0d0)
    open(29,file="NAMD.cc_out",form="unformatted")
    rewind(29)
    ! read 0-th MD step cc_out
    ! first step cc is written instead of DM (i.e. only 1D array is written)
    read(29) istep_t,nstate_t
    if(nstate_t.ne.nstate) then
        write(6,*) "nstates in NAMD.cc_out,NAMD.input not the same", nstate_t,nstate
        stop
    endif
    read(29) E1      ! eV unit
    read(29) cc_int
    read(29) cphase
!   read(29) Hij_R
    forall(m=1:nstate) DM(m,m)=cc_int(m)
 



    ! read OUT.NAMD_update
    open(12,file="OUT.NAMD_update",form="unformatted")
    rewind(12)
    !
    allocate(cdot(nstate,nstate))
    allocate(E2(nstate))
    !
    ! read OUT.NAMD_update initial step
    read(12) istep_t,islda,nkpt    ! istep2=0 for t=0fs MD
    if(islda.ne.1.or.nkpt.ne.1) then
        write(6,*) 'currently, only works for islda=1,nkpt=1',islda,nkpt
        stop
    endif
    do iislda=1,islda
        do kpt=1,nkpt
            read(12) kpt_t,iislda_t,mst_win
            if(mst_win.ne.nstate) then
              write(6,*) "nstate.ne.mst_win",nstate,mst_win
              stop
            endif
            read(12) E2   ! hartree unit
        enddo
    enddo




    if (restart_istep==0) then

        ! sum DM for initial step
        !
        write(6,*) 'summing over Dij for initial step'
        rho_tmp2=0d0
        rhor=0d0; rhoi=0d0
!$OMP PARALLEL SHARED(n1,n2,n3,nstate,DM,rho_r,rho_i,nthread,chunk)  &
!$OMP PRIVATE(m,n,i,j,k)  &
!$OMP REDUCTION(+:rho_tmp2)
        nthread = OMP_GET_MAX_THREADS()
        chunk = ceiling( (nstate)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
        do m=1,nstate
            do n=m,nstate
                if (m==n) then
                    rho_tmp2=rho_tmp2+dble( DM(m,n)*((rho_r(:,:,:,m)+comi*rho_i(:,:,:,m))*(rho_r(:,:,:,n)-comi*rho_i(:,:,:,n))) )
                else
                    rho_tmp2=rho_tmp2+2d0*dble( DM(m,n)*((rho_r(:,:,:,m)+comi*rho_i(:,:,:,m))*(rho_r(:,:,:,n)-comi*rho_i(:,:,:,n))) )
                end if
            end do
        end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL
        !
        forall(i=1:n1,j=1:n2,k=1:n3) rhor(i,j,k)=real(rho_tmp2(i,j,k))
        !
        sum_r=sum(rhor)
        write(6,*) 'Real part summation is ', sum_r*vol/(n1*n2*n3)
        write(6,*) 'Highest is ', maxval(rhor)*vol/(n1*n2*n3)
        write(6,*) 'Lowest is ', minval(rhor)*vol/(n1*n2*n3)

        ! customize charge analysis (different for different systems)
        write(6,*) 'charge percentage for Au is: '
        sum_r=sum(rhor(:,:,67:130))
        write(6,*) sum(rhor(:,:,:))*vol/(n1*n2*n3)
        write(6,*) sum(rhor(:,:,67:130))*vol/(n1*n2*n3), (sum(rhor(:,:,1:66))+sum(rhor(:,:,131:)))*vol/(n1*n2*n3)
        write(6,*) sum_r*vol/(n1*n2*n3)

        ! write out to .xsf format
        WRITE( istep_index, FMT = '( I4.4 )' ) 0
        open(22,file='istep-'//trim(istep_index)//'-psi.xsf')
        rewind(22)
        write(22,'(x,a)') 'CRYSTAL'
        write(22,'(x,a)') 'PRIMVEC'
        do i = 1, 3
            write (22, '(3f14.10)') AL(1:3,i)*A_AU_1
        end do
        write(22,'(x,a)') 'PRIMCOORD'
        write(22, *) natom, 1
        do iat=1,natom
            write(22, '(x,i3,3f14.10)') iz(iat),xyz(1:3,iat)
        end do
        write(22,1001)"BEGIN_BLOCK_DATAGRID_3D"
        write(22,1001)"XSF_FILE"
        write(22,1001)" BEGIN_DATAGRID_3D_XSF_FILE"
        write(22,98) n1,n2,n3
        write(22,200) 0.0000,0.00000,0.00000
        write(22,200) AL(1,1)*A_AU_1,AL(2,1)*A_AU_1,AL(3,1)*A_AU_1
        write(22,200) AL(1,2)*A_AU_1,AL(2,2)*A_AU_1,AL(3,2)*A_AU_1
        write(22,200) AL(1,3)*A_AU_1,AL(2,3)*A_AU_1,AL(3,3)*A_AU_1
        write(22,*)
        write(22,100) (((rhor(i,j,k),i=1,n1),j=1,n2),k=1,n3)

        write(22,1001)"END_DATAGRID_3D"
        write(22,1001)"END_BLOCK_DATAGRID_3D"
        close(22)

    end if



    ! MD loop
    do istep=1,nstep

        write(6,*) 
        write(6,*) 'Working on MD step ', istep


        ! read cdot from NAMD
        read(12) istep_t,islda,nkpt
        do iislda=1,islda
            do kpt=1,nkpt
                read(12) kpt_t,iislda_t,mst_win
                read(12) cdot
                read(12) E2    ! hartree unit
            enddo
        enddo
        write(6,*) 'cdot ', istep_t


        ! if istep is at step with mod(step,ngio)==0, read ugio;
        !   otherwise, interpolate the adiabatic states based on cdot
        if (mod(istep,ngio)==0) then

            ! read ugio_all
            ug_tot=cmplx(0d0,0d0)
            num=0
            read(10) istep_t,mg_nx_t,mst,nnodes,nkpt,islda,kpt,iislda
            do inode=1,nnodes
                do m=1,mst
                    read(10) ug_n_tmp(:)
                    if (m>=ibnd_1 .and. m<=ibnd_2) then
                        do ig=1,ngtotnod_9(inode,1)   ! only one kpoint
                            ug_tot(num+ig,m-ibnd_1+1)=ug_n_tmp(ig)
                        enddo
                    end if
                enddo
                num=num+ngtotnod_9(inode,1)
            enddo
            write(6,*) 'extracted istep of MD from ugio_all is ', istep_t

        else

            if (istep >= int(restart_istep/ngio)*ngio) then
                !
                ! cdot is from OUT.NAMD_update (cdot=<phi_T1|phi_T2>), enforce unitarity of cdot, 
                !   then, use cdot and phi_T1 to reconstruct phi_T2
                ! ortho cdot (hole case) **NOTE** please add electron case if needed
                do j2=1,nstate
                    do j1=1,j2-1
                        cc3=dcmplx(0.d0,0.d0)
                        do i=1,nstate
                            cc3=cc3+dconjg(cdot(i,nstate-j1+1))*cdot(i,nstate-j2+1)
                        enddo
                        do i=1,nstate
                            cdot(i,nstate-j2+1)=cdot(i,nstate-j2+1)-cc3*cdot(i,nstate-j1+1)
                        enddo
                    enddo
                    sum_cc=0.d0
                    do i=1,nstate
                        sum_cc=sum_cc+real(cdot(i,nstate-j2+1)*dconjg(cdot(i,nstate-j2+1)))
                    enddo
                    sum_cc=1.d0/dsqrt(sum_cc)
                    do i=1,nstate
                        cdot(i,nstate-j2+1)=sum_cc*cdot(i,nstate-j2+1)
                    enddo
                enddo
                !
                ! mat multiplication
                ng=size(ug_tot,1)
                ug_tot_tmp=cmplx(0d0,0d0)
                ug_tot=c_matmul(ng,nstate,ug_tot,cdot)
                !
                ! check and re-set normalization of the reconstructed adiabatic states
                write(98,*) istep
                do m=1,nstate
                    inner=dot_product(ug_tot(:,m),ug_tot(:,m))*vol
                    write(98,*) m, inner
                    ug_tot(:,m)=ug_tot(:,m)/inner
                end do

            end if

        end if   
    



        ! read cdot from cc_out
        read(29) istep_t,nstate_t
        read(29) E1
        read(29) DM
        read(29) Pij
        read(29) cphase
!       read(29) Hij_R
        icount_state=icount_state+1
        write(6,*) 'extracted istep of MD from cc_out is ', istep_t
        if (istep_t /= istep) then
            write(6,*) 'Inconsistent index of extraction for cc_out and istep, exit'
            stop
        end if
        write(6,*) 'cc_out ', istep_t




        ! read MOVEMENT
        read(15,*) natom_t
        read(15,*) 
        read(15,*) AL_t(1,1),AL_t(2,1),AL_t(3,1)
        read(15,*) AL_t(1,2),AL_t(2,2),AL_t(3,2)
        read(15,*) AL_t(1,3),AL_t(2,3),AL_t(3,3)
        read(15,*) 
        do iat=1,natom
            read(15,*) iz(iat),xatom(1,iat),xatom(2,iat),xatom(3,iat)
        enddo
        read(15,*)  ! force
        do iat=1,natom
            read(15,*)   ! force
        enddo
        read(15,*)  ! Velocity
        do iat=1,natom
            read(15,*)   ! Velocity
        enddo
        read(15,*)    ! -------------------
    
        do iat=1,natom
            do j=1,3
                xatom(j,iat)=mod(xatom(j,iat)+2.d0,1.d0)
            enddo
        enddo
    
        do iat=1,natom
            do i=1,3
                xyz(i,iat)=AL(i,1)*A_AU_1*xatom(1,iat)+AL(i,2)*A_AU_1*xatom(2,iat)+ AL(i,3)*A_AU_1*xatom(3,iat)
            enddo
        enddo





        ! summation and print out wavefunction

        !   RHO = \sum_i,j D_i,j conjg(\phi_i) * \phi_j * conjg(cphase(i))*cphase(j)
        !     first FFT wfc to real space, then perform Dij summation
        !   sum over R space and state m,n to get charge density
        
        if (istep>=restart_istep .and. mod(istep,n_sum)==0) then

            write(6,*) 'start to do FFT'
            rho_r=0d0; rho_i=0d0
!$OMP PARALLEL SHARED(nstate,ng_tot,n1,n2,n3,AL,gkk_x,gkk_y,gkk_z,rho_r,rho_i,cphase,ug_tot,nthread,chunk)  &
!$OMP PRIVATE(m,ig,aa1,aa2,aa3,i1,i2,i3,index_ig,lwrk,wrk) 
            nthread = OMP_GET_MAX_THREADS()
            chunk = ceiling( nstate/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK) 
            do m=1,nstate
                lwrk=6*(n1+n2+n3)+15
                if (.not. allocated(wrk)) allocate(wrk(lwrk))
                if (.not. allocated(index_ig)) allocate(index_ig(n1,n2,n3))
                index_ig=0
                do ig=1,ng_tot
                    aa1=AL(1,1)*gkk_x(ig)+AL(2,1)*gkk_y(ig)+AL(3,1)*gkk_z(ig)
                    aa2=AL(1,2)*gkk_x(ig)+AL(2,2)*gkk_y(ig)+AL(3,2)*gkk_z(ig)
                    aa3=AL(1,3)*gkk_x(ig)+AL(2,3)*gkk_y(ig)+AL(3,3)*gkk_z(ig)
                    aa1=aa1/(2*pi)+n1*2+0.1
                    aa2=aa2/(2*pi)+n2*2+0.1
                    aa3=aa3/(2*pi)+n3*2+0.1
                    i1=aa1
                    i2=aa2
                    i3=aa3
                    i1=mod(i1,n1)+1
                    i2=mod(i2,n2)+1
                    i3=mod(i3,n3)+1
                    if(index_ig(i1,i2,i3).gt.0) then
                        write(6,*) "something wrong,two ig,samei1,i2,i3", aa1,aa2,aa3,i1,i2,i3
                        stop
                    endif
                    index_ig(i1,i2,i3)=1
                    rho_r(i1,i2,i3,m)=real(ug_tot(ig,m)*cphase(m))
                    rho_i(i1,i2,i3,m)=aimag(ug_tot(ig,m)*cphase(m))
                end do
                wrk=0d0
                call cfft(n1,n2,n3,rho_r(:,:,:,m),rho_i(:,:,:,m),wrk,lwrk,1)
            end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL


            ! analyze adiabatic state
            write(106,*) '#, m- state, total charge, Au charge, GaN charge', istep
            do m=1,nstate
                rhor=dble(rho_r(:,:,:,m)**2+rho_i(:,:,:,m)**2)
                write(106,'(I,4F20.12)') m, sum(rhor)*vol/(n1*n2*n3), sum(rhor(:,:,67:130))*vol/(n1*n2*n3), (sum(rhor(:,:,1:66))+sum(rhor(:,:,131:)))*vol/(n1*n2*n3)
            end do
 

            ! sum over state m,n with D_mn, which gives charge density of time-dependent wavefunction
            !
            write(6,*) 'summing over Dij'
            rho_tmp2=0d0
            rhor=0d0; rhoi=0d0
!$OMP PARALLEL SHARED(n1,n2,n3,nstate,DM,rho_r,rho_i,nthread,chunk)  &
!$OMP PRIVATE(m,n,i,j,k)  &
!$OMP REDUCTION(+:rho_tmp2)
            nthread = OMP_GET_MAX_THREADS()
            chunk = ceiling( (nstate)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK)
            do m=1,nstate
                do n=m,nstate
                    if (m==n) then
                        rho_tmp2=rho_tmp2+dble( DM(m,n)*((rho_r(:,:,:,m)+comi*rho_i(:,:,:,m))*(rho_r(:,:,:,n)-comi*rho_i(:,:,:,n))) )
                    else
                        rho_tmp2=rho_tmp2+2d0*dble( DM(m,n)*((rho_r(:,:,:,m)+comi*rho_i(:,:,:,m))*(rho_r(:,:,:,n)-comi*rho_i(:,:,:,n))) )
                    end if
                end do
            end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL
            !
            forall(i=1:n1,j=1:n2,k=1:n3) rhor(i,j,k)=real(rho_tmp2(i,j,k))


            ! customized charge analysis (different for different systems)
            sum_r=sum(rhor)
            write(6,*) 'Real part summation is ', sum_r*vol/(n1*n2*n3)
            write(6,*) 'Highest is ', maxval(rhor)*vol/(n1*n2*n3)
            write(6,*) 'Lowest is ', minval(rhor)*vol/(n1*n2*n3)

            write(6,*) 'charge percentage for Au is: '
            sum_r=sum(rhor(:,:,67:130))
            write(6,*) sum(rhor(:,:,:))*vol/(n1*n2*n3)
            write(6,*) sum(rhor(:,:,67:130))*vol/(n1*n2*n3), (sum(rhor(:,:,1:66))+sum(rhor(:,:,131:)))*vol/(n1*n2*n3)
            write(6,*) sum_r*vol/(n1*n2*n3)
            



            ! write out for .xsf format
            WRITE( istep_index, FMT = '( I4.4 )' ) istep
            open(22,file='istep-'//trim(istep_index)//'-psi.xsf')
            rewind(22)
            write(22,'(x,a)') 'CRYSTAL'
            write(22,'(x,a)') 'PRIMVEC'
            do i = 1, 3
                write (22, '(3f14.10)') AL(1:3,i)*A_AU_1
            end do
            write(22,'(x,a)') 'PRIMCOORD'
            write(22, *) natom, 1
            do iat=1,natom
                write(22, '(x,i3,3f14.10)') iz(iat),xyz(1:3,iat)
            end do
            write(22,1001)"BEGIN_BLOCK_DATAGRID_3D"
            write(22,1001)"XSF_FILE"
            write(22,1001)" BEGIN_DATAGRID_3D_XSF_FILE"
            write(22,98) n1,n2,n3
            write(22,200) 0.0000,0.00000,0.00000
            write(22,200) AL(1,1)*A_AU_1,AL(2,1)*A_AU_1,AL(3,1)*A_AU_1
            write(22,200) AL(1,2)*A_AU_1,AL(2,2)*A_AU_1,AL(3,2)*A_AU_1
            write(22,200) AL(1,3)*A_AU_1,AL(2,3)*A_AU_1,AL(3,3)*A_AU_1
            write(22,*)
            write(22,100) (((rhor(i,j,k),i=1,n1),j=1,n2),k=1,n3)
            !
            write(22,1001)"END_DATAGRID_3D"
            write(22,1001)"END_BLOCK_DATAGRID_3D"
            close(22)

        end if


    end do 


    98     format(3(i5,2x))
    200    format(3(1x,E13.5,1x))
    100    format(6(E14.5))
    1001   format(a28)


contains


    function c_matmul(n1,n2,v,H)
      integer                              :: n1, n2
      complex*16,dimension(n1,n2)          :: v
      complex*16,dimension(n2,n2)          :: H
      complex*16,dimension(n1,n2)          :: c_matmul

      integer                              :: i, j, k, nthread, chunk
      complex*16                           :: csum
      complex*16,dimension(n1,n2)          :: H_tmp

!$OMP PARALLEL SHARED(n1,n2,v,H,H_tmp,nthread,chunk)  &
!$OMP PRIVATE(i,j,k,csum)
      nthread = OMP_GET_MAX_THREADS()
      chunk = ceiling( (n1*n2)/float(nthread) )
!$OMP DO SCHEDULE(DYNAMIC,CHUNK) COLLAPSE(2)
      do i=1,n1
          do j=1,n2
              csum=cmplx(0d0,0d0)
              do k=1,n2
                  csum=csum+v(i,k)*H(k,j)
              end do
              H_tmp(i,j)=csum
          end do
      end do
!$OMP END DO NOWAIT
!$OMP END PARALLEL

      forall(i=1:n1,j=1:n2) c_matmul(i,j)=H_tmp(i,j)

    end function



 
end program NAMD_psi
